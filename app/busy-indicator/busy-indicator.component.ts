import {Component, Input} from '@angular/core';

@Component({
	styleUrls: ['app/busy-indicator/busy-indicator.component.css'],
	templateUrl: 'app/busy-indicator/busy-indicator.component.html',
	selector: 'busy-indicator'
})
export class BusyIndicatorComponent{
	_loader: Promise<any>;
	get loader(): Promise<any> { return this._loader; }
	@Input() 
	set loader(loader: Promise<any>) {
		try{
			this._loader = loader;
			if(this._loader){
				this._loader.then(()=> setTimeout(function(){
					this._loader = null;
				}.bind(this), 250))
									 .catch((reason: any)=> setTimeout(function(){
					this._loader = null;
				}.bind(this), 250));
			}
		} catch(e) {
			console.log(e);
		}
	}
}
