import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Model } from '../model/model';
import { ModelService } from '../model/model.service';

interface MyEventTarget extends EventTarget {
  scrollTop: number,
	offsetHeight: number,
	scrollHeight: number
}

interface MyEvent extends Event {
    target: MyEventTarget;
}

@Component({
	moduleId: module.id,
	styleUrls: ['./master.component.css', '../styles/toolbar.css'],
	templateUrl: './master.component.html',
	selector: "master"
})
export class MasterComponent implements OnInit {
	models: any[];
	selectMode: any;
	countSelected: any;
	step: number;
	count: number;
	loader: Promise<any>;
	backLoader: Promise<any>;
	backLoaderStatus: string;
	
	constructor( public router: Router,
							 private modelService: ModelService){
		this.selectMode = { 
			mode: 'select-single' 
		};
		
		this.step = 25;
		this.count = 0;
		this.models = [];

		this.countSelected = {
			value: 0
		};
	}
	
	onSelect(model: any): void {
		try{
			model.isSelected = !model.isSelected;
			if(model.isSelected){
				++this.countSelected.value;
			}else {
				--this.countSelected.value;
			}
	 
			if(this.selectMode.mode == 'select-single'){
				this.loader = this.navigateTo(model.id);
				this.loader.catch(function(reason){
					console.log(reason);
				});
			
				this.models.filter( f=> f.isSelected == true && f != model )
									 .forEach(function(model){ delete model.isSelected; });
			}
		} catch(e) {
			console.log(e);
		}
	}

	navigateTo(id?: number) : Promise<any> {
		try {
			let params: any[] = [];

			if(this.router.url.startsWith('/models')) {
				params.push('/models');
			}else {
				params.push('/mobile/models');
			}

			if(id || id === 0){
				params.push(id);
			}

			return this.router.navigate.apply(this.router, [ params ]).catch(function(reason: any){
				console.log(reason);
			});
		} catch(e) {
			console.log(e);
		}
	}

	onScroll(event: MyEvent) {
		try {
			if(this.models.length < this.count) {
				if(((event.target.scrollTop + 2 * event.target.offsetHeight) > event.target.scrollHeight)
						&& this.backLoaderStatus != 'pending' ) {
					this.getModelsAsync();
				}else {
					
				}
			} else {
				event.preventDefault();
				event.stopPropagation();
			}
		} catch(e) {
			console.log(e);
		}
	}

	getModelsAsync(): void {
		try {
			let listPromise = this.modelService.getAsync('api/models', {
				'$skip': this.models.length, 
				'$take': this.step 
			});
			
			if(this.count) {
				this.backLoaderStatus = 'pending';
				this.backLoader = listPromise;
				this.backLoader.then( models => { 
					this.backLoaderStatus = 'finished';
					this.models.push.apply(this.models, models);
				}).catch(function(reason){
					this.backLoaderStatus = 'finished';
					console.log(reason);
				});
			} else {
				let countPromise = this.modelService.getAsync('api/models/$count');

				this.loader = Promise.all([listPromise, countPromise]);
				this.loader.then( values => {
					this.models.push.apply(this.models, values[0]);
					this.count = values[1];
				})				 .catch(function(reason: any){
					console.log(reason);
				});
			}
		} catch(e) {
			console.log(e);
		}
  }  
	
	ngOnInit(): void {
		try{ 
			this.getModelsAsync();
		} catch(e) {
			console.log(e);
		}
  }
	
	create(): void {
		try {
			this.loader = this.router.navigate(['/models/create' ]);
			this.loader.then(() => this.models.forEach(function(model){ delete model.isSelected; }));
			this.loader.catch(function(reason: any){
				console.log(reason);
			});
		} catch(e) {
			console.log(e);
		}
	}
	
	deleteSelected(): void {
		let selectedModels = this.models.filter( model=> model.isSelected == true ),
				promises: Promise<any>[] = [],
				id: number;

		if(this.router['currentUrlTree'].root.children.primary.segments.length === 2) {
			id = parseInt(this.router['currentUrlTree'].root.children.primary.segments[1].path);
		}

		selectedModels.forEach(function(model: any){
			promises.push(
				this.modelService.deleteAsync(`api/models/${model.id}`)
			);
			if(model.id === id){
				promises.push(this.navigateTo());
			}
		}.bind(this));

		this.loader = Promise.all(promises);
		this.loader.then(() => {
			this.models = this.models.filter( f => selectedModels.indexOf(f) === -1 );
		})					.catch(function(reason: any){
			console.log(reason);
		});
	}

	exportSelected(): void {
		try {
			let selectedModels = this.models.filter( model=> model.isSelected == true );
			let csvContent: string = "";
		  let link = document.createElement("a");
			let columns: string[];

			if(!selectedModels) {
				throw "no items selected"
			}

			//export column names
			columns = Object.keys(selectedModels[0]);
			columns.forEach(function(name: string, index: number){
				if(index) {
					csvContent += ";" + name;
				} else {
					csvContent += name;
				}
			});
			csvContent += "\r\n";

			//export data
			selectedModels.forEach(function(model: any){
				columns.forEach(function(name: string, index: number){
					if(index) {
						csvContent += ";" + model[name];
					} else {
						csvContent += model[name];
					}
				});
				csvContent += "\r\n";
			});

		  if (link.download !== undefined) // feature detection
		  { 
				let blob = new Blob([csvContent], { "type": 'text/csv;charset=utf-8;' });
		    // Browsers that support HTML5 download attribute
		    let url = URL.createObjectURL(blob); 
				let filename = "export" + new Date() + ".csv";
 
		    link.setAttribute("href", url);
		    link.setAttribute("download", filename);
		    link.style.visibility = 'hidden';
		    document.body.appendChild(link);
		    link.click();
		    document.body.removeChild(link);
		  }
		} catch(e) {
			console.log(e);
		}
	}

  switchSelectMode(mode: any) {
		if(this.selectMode.mode == 'select-multi' && mode == 'select-single' ) {
			this.models.forEach(function(model: any){
				model.isSelected = false;
			});
		}

    this.selectMode.mode = mode;
	}

}
