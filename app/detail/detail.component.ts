import { Component, OnInit, Input, ViewChild } 				from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { Validators, NgForm } from '@angular/forms'; 

import 'rxjs/add/operator/switchMap';

import { Model } from '../model/model';
import { ModelService } from '../model/model.service';

@Component({
  moduleId: module.id,
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css', '../styles/toolbar.css']
})
export class DetailComponent implements OnInit {
	model: Model;
	loader: Promise<any>;
	metadata: any;
	keys: string[];
	mode: string;
	@ViewChild(NgForm) form: NgForm;
	
	constructor( private modelService: ModelService,
		 					 public route: ActivatedRoute,
							 private location: Location ) {
		this.mode = 'show';
	}
	
	ngOnInit(): void {
		try {
			let promises: Promise<any>[] = [],
					promise: Promise<any>;
					
			this.route.params
				.switchMap((params: Params) => {
					
					if(params['id'] == 'create'){
						this.mode = 'create';
						this.model = {
							id: 0,
							name: ''
						};
						
					} else {			
						this.mode = 'show';			 				
						promise = this.modelService.getAsync(`api/models/${+params['id']}`);
						promise.then((model: any) => { 
							this.model = model;					
							//this.form.reset();
						});
						promises.push(promise);
							
					}
					
					if(!this.metadata){						
						promise = this.modelService.getAsync('api/models/$metadata');
						promise.then((metadata: any) => this.metadata = metadata );
						promises.push(promise);
					}
					
					this.loader = Promise.all(promises);
					this.loader.catch(function(reason){
						console.log(reason);
					});
					
					return this.loader;
				})
				.subscribe(values => { 
					this.keys = Object.keys(this.model).filter( key => this.metadata.properties[key][this.mode] != 'hidden');
				});

			

		} catch(e) {
			console.log(e);
		}
	}
	
	switchEditMode(mode: string): void {
		this.mode = mode;
		this.keys = Object.keys(this.model).filter( key => this.metadata.properties[key][this.mode] != 'hidden');
	}
	
	delete(): void {
		this.loader = this.modelService.deleteAsync(`api/models/${this.model.id}`)
		this.loader.then(() => this.goBack()).catch(function(reason: any){
			console.log(reason);
		});
	}
	
	goBack(): void {
		this.location.back();
	}
	
	save(smartForm:any): void {
		try {
		this.route.params
			.switchMap((params: Params) => this.modelService.update(`api/models/${+params['id']}`, this.model))
			.subscribe(() => this.goBack());
		} catch(e) {
			console.log(e);
		}
	}

}
