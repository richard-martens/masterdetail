import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
	templateUrl: 'main-nav.component.html',
	styleUrls: ['main-nav.component.css'],
	selector: 'main-nav'
})
export class MainNavComponent {
	
	constructor( ) {
	}
	
}
