import { Component } from '@angular/core';
import { Location } from '@angular/common';

import { Model } from '../model/model';
import { ModelService } from '../model/model.service';

@Component({
	moduleId: module.id,
	templateUrl: './import.component.html',
	styleUrls: ['./import.component.css', '../styles/toolbar.css', '../detail/detail.component.css' ]
})
export class ImportComponent {
	loader: Promise<any>;
	metadata: any;
	mode: string;
	model: any;
	keys: string[];
	
	constructor( private modelService: ModelService,
							 private location: Location ) {
		this.metadata = {
			properties: {
				file : {
						type: 'file',//text, password, datetime-local, date, month, time, 
													//week, number, email, url, search, tel, color
						label: 'File:',
						isKey: true,
						edit: 'required',// optional, possible values: hidden, required, readonly, undefined=optional
				}
			}
		};
		
		this.mode = 'edit';
		this.model = {
		};
		
		this.keys = [ 'file' ];
	}
	
	goBack(): void {
		this.location.back();
	}
	
	import_file(smartForm:any): void {
		try {
			
		} catch(e) {
			console.log(e);
		}
	}

}
