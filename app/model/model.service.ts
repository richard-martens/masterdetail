import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Model } from '../model/model';

@Injectable()
export class ModelService {
	
	constructor(private http: Http) { }

  getAsync(url: string, search?: any): Promise<any> {
		let params = new URLSearchParams();

    for(let key in search) {
      params.set(key, search[key]);
    }
    let options = new RequestOptions({
        search: params
    });

    return this.http.get(url, options)
               .toPromise()
               .then(response => response.json().data)
               .catch(this.handleError);
	} 
	
	private headers = new Headers({'Content-Type': 'application/json'});

	update(url: string, model: Model): Promise<Model> {
		return this.http
			.put(url, JSON.stringify(model), {headers: this.headers})
			.toPromise()
			.then(() => model)
			.catch(this.handleError);
	}
	
	create(url: string, model: Model): Promise<Model> {
		return this.http
			.post(url, JSON.stringify(model), {headers: this.headers})
			.toPromise()
			.then(res => res.json().data)
			.catch(this.handleError);
	}

	deleteAsync(url: string): Promise<void> {
		return this.http.delete(url, {headers: this.headers})
			.toPromise()
			.then(() => null)
			.catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

