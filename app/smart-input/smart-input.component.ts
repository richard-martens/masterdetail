import { Component, Input, Output } 				from '@angular/core';

@Component({
	styleUrls:['app/smart-input/smart-input.component.css'],
	templateUrl:'app/smart-input/smart-input.component.html',
	selector:'smart-input'
})
export class SmartInputComponent{
	@Input() metadata: any;
	@Input() @Output() model: any;
	@Input() name: string;
	@Input() mode: string;
}
