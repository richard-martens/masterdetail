import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }  from './app.component';
import { MasterDetailComponent } from '../master-detail/master-detail.component';
import { MasterComponent } from '../master/master.component';
import { DetailComponent } from '../detail/detail.component';
import { ImportComponent } from '../import/import.component';


const routes: Routes = [
	{ path: 'models/import', component: ImportComponent },
	{	path: 'models',	component: MasterDetailComponent, children:[
		{ path: ':id', component: DetailComponent }
	]},
	{ path: 'mobile/models', component: MasterComponent },
	{ path: 'mobile/models/:id', component: DetailComponent },
	{ path: '', redirectTo: window.innerWidth >= 768 ? '/models' : '/mobile/models', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
