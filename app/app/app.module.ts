import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ModelService } from '../model/model.service';
import { AppRoutingModule }     from './app-routing.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from '../in-memory-data/in-memory-data.service';

import { AppComponent }  from './app.component';
import { MasterDetailComponent } from '../master-detail/master-detail.component';
import { MasterComponent } from '../master/master.component';
import { DetailComponent } from '../detail/detail.component';
import { ImportComponent } from '../import/import.component';

import { MainNavComponent } from '../main-nav/main-nav.component';
import { BusyIndicatorComponent } from '../busy-indicator/busy-indicator.component';
import { SmartInputComponent } from '../smart-input/smart-input.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, { delay: 1000 }),
		AppRoutingModule,
		NgbModule.forRoot()
  ],
  declarations: [
    AppComponent, BusyIndicatorComponent, DetailComponent, ImportComponent, MainNavComponent, MasterComponent, MasterDetailComponent, SmartInputComponent
  ],
	providers: [ ModelService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
