import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	moduleId: module.id,
	styleUrls: ['./master-detail.component.css', '../styles/toolbar.css'],
	templateUrl: './master-detail.component.html' 
})
export class MasterDetailComponent {
	constructor( public router: Router) {
		
	}
}
