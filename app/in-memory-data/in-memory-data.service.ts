import { RequestMethod, Response, ResponseOptions, URLSearchParams } from '@angular/http';

import { createErrorResponse, emitResponse, InMemoryDbService, HttpMethodInterceptorArgs,
				 ParsedUrl, RequestInfo, STATUS } from 'angular-in-memory-web-api';

import { Observable } from 'rxjs/Observable';
import { Observer }   from 'rxjs/Observer';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let models = this.generateTestData();

		models['$metadata'] = this.getMetadata();
		

    return {models};
  }
	
	private generateTestData(): any[] {
		let list: any = [],
				item: any,
				metadata = this.getMetadata(),
				properties = Object.keys(metadata.properties),
				columnMeta: any;
		
		for(let i=0; i < 100; ++i){
			item = {};
				
			properties.forEach(function(column: any, index: number){
				columnMeta = metadata.properties[column];
				
				switch(columnMeta.type){
					case 'number':	
						if(columnMeta.type === 'number' && columnMeta.isKey){
							item[column] = i;
						}else{
							item[column] = Math.floor((Math.random() * Number.MAX_VALUE) + 1);
						}
						break;

					case 'text':
						var pos : number = Math.floor((Math.random() * 100) + 1),
						text: string = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
						item[column] = text.substring(pos, 100);
						break;

					default: throw `Type ${columnMeta.type} is not supported`;
				}
				
			});
			list.push(item);
		}
		return list;
	}
	
	private getMetadata(): any {
		return {
			properties: {
				id : {
						type: 'number',//text, password, datetime-local, date, month, time, 
													//week, number, email, url, search, tel, color
						label: '#',
						isKey: true,
						create: 'hidden', // optional, possible values: hidden, required, undefined=optional
						edit: 'readonly',// optional, possible values: hidden, required, readonly, undefined=optional
						show: 'readonly'// optional, possible values: hidden, required, readonly, undefined=optional
				},
				name : {
					type: 'text',
					label: 'Name',
					create: 'required', 
					edit: 'required',
					show: 'readonly'
				}
			}
		};
	}


  // HTTP GET interceptor
  protected get(interceptorArgs: HttpMethodInterceptorArgs) {
    // Returns a "cold" observable that won't be executed until something subscribes.
    return new Observable<Response>((responseObserver: Observer<Response>) => {
      let resOptions: ResponseOptions;

      const {id, query, collection, collectionName, headers, req} = interceptorArgs.requestInfo;
      let data = collection;

      if (id || id === 0) {
        data = this.findById(collection, id);
      } else if (query) {
        data = this.applyQuery(collection, query);
      }

      if (data) {
        resOptions = new ResponseOptions({
          body: { data: this.clone(data) },
          headers: headers,
          status: STATUS.OK
        });
      } else {
        resOptions = createErrorResponse(req, STATUS.NOT_FOUND,
           `'${collectionName}' with id='${id}' not found`);
      }

      emitResponse(responseObserver, req, resOptions);
      return () => { }; // unsubscribe function
    });
  }
	
  /////////// private ///////////////
  private applyQuery(collection: any[], query: URLSearchParams) {
		let skip: number, take: number;
    // extract filtering conditions - {propertyName, RegExps) - from query/search parameters
    const conditions: {name: string, rx: RegExp}[] = [];
    const caseSensitive = 'i';

    query.paramsMap.forEach((value: string[], name: string) => {
			if(name == '$skip') skip = parseInt(value[0]);
			else if(name == '$take') take = parseInt(value[0]);
      else value.forEach(v => conditions.push({name, rx: new RegExp(decodeURI(v), caseSensitive)}));
    });
		
		if(skip){
			collection = collection.slice(skip);
		}

		if(take){
			collection = collection.slice(0, take);
		}

    const len = conditions.length;
    if (!len) { return collection; }

    // AND the RegExp conditions
    return collection.filter(row => {
      let ok = true;
      let i = len;
      while (ok && i) {
        i -= 1;
        const cond = conditions[i];
        ok = cond.rx.test(row[cond.name]);
      }
      return ok;
    });
  }


  private clone(data: any) {
    return JSON.parse(JSON.stringify(data));
  }

  private findById(collection: any, id: number | string) {
		if(id == '$count') return collection.length;
		else if(id == '$metadata') return collection['$metadata'];
    else return collection.find((item: any) => item.id === id);
  }
}
