# Master/Detail CRUD template

## Current status

The template is being developed. Not all the necessary functions are implemented yet.

### Planed features

* Import (not completed)

Import entities from csv-file. View and component created. Import logic is not implemented.

* Export (not completed)

Export selected entities to csv-file is completed. select all entities button is missed(not implemented).

* Create entity (not completed)
Creating entity is completed, but has bugs. Smartform template reference variable is not recognized. Another problem ist smart-input is not completed. It synced its state not with the form and it didn't reset the state after navigation.

* Update entity (not completed)

Same state as Create entity, because it is the same view.

* Show entity (completed)
* Delete entity (completed)
* Deleted multiple entities (completed)
* SmartInput Direktive(not completed)

This directive renders the html control described in metadata of web oapi. The support for follow types are implemented:

* Number
* Text
* File

* web oapi(not completed)

Only client side components are completed. That are descriptions of model with view depended attributes.

* InMemory Web oapi(completed)

Extended angular inMemory API for support oapi. See oapi definition in wiki.

* test data generator (not completed)

Test data generator is included in InMemory web oapi. It generates test data which corresponds to metadata.

* web request batcher( not completed)

Web request batcher register the incoming web request between interrupts. It can configured to pass trough only unique request by resources URL. After an interrupt it creates a single batch request to remote host.

* Localization (not completed)

* server web oapi component(not completed)

* full offline support (not completed)



## Partner wanted

I am searching for partners for planning and implementation of complex business applications.

## Prerequisites

Node.js and npm are essential to Angular development. 

[Get it now](https://docs.npmjs.com/getting-started/installing-node) if it's not already installed on your machine.    
 
**Verify that you are running at least node `v4.x.x` and npm `3.x.x`**
by running `node -v` and `npm -v` in a terminal/console window.
Older versions produce errors.

We recommend [nvm](https://github.com/creationix/nvm) for managing multiple versions of node and npm.

## Create a new project based on this template

I do not know that yet. I'm looking for a way to create new applications without to copy the same source code ever.

## Install npm packages

Install the npm packages described in the `package.json` and verify that it works:

```bash
npm install
npm start
```

The `npm start` command first compiles the application, 
then simultaneously re-compiles and runs the `lite-server`.
Both the compiler and the server watch for file changes.

Shut it down manually with `Ctrl-C`.

### npm scripts

We've captured many of the most useful commands in npm scripts defined in the `package.json`:

* `npm start` - runs the compiler and a server at the same time, both in "watch mode".
* `npm run tsc` - runs the TypeScript compiler once.
* `npm run tsc:w` - runs the TypeScript compiler in watch mode; the process keeps running, awaiting changes to TypeScript files and re-compiling when it sees them.
* `npm run lite` - runs the [lite-server](https://www.npmjs.com/package/lite-server), a light-weight, static file server, written and maintained by
[John Papa](https://github.com/johnpapa) and
[Christopher Martin](https://github.com/cgmartin)
with excellent support for Angular apps that use routing.

Here are the test related scripts:
* `npm test` - compiles, runs and watches the karma unit tests
* `npm run e2e` - run protractor e2e tests, written in JavaScript (*e2e-spec.js)

## Testing

This repo adds both karma/jasmine unit test and protractor end-to-end testing support.

These tools are configured for specific conventions described below.

*It is unwise and rarely possible to run the application, the unit tests, and the e2e tests at the same time.
I recommend that you shut down one before starting another.*

### Unit Tests
TypeScript unit-tests are usually in the `app` folder. Their filenames must end in `.spec`.

Look for the example `app/app/app.component.spec.ts`.
Add more `.spec.ts` files as you wish; karma is configured to find them.

Run it with `npm test`

That command first compiles the application, then simultaneously re-compiles and runs the karma test-runner.
Both the compiler and the karma watch for (different) file changes.

Shut it down manually with `Ctrl-C`.

Test-runner output appears in the terminal window.
We can update our app and our tests in real-time, keeping a weather eye on the console for broken tests.
Karma is occasionally confused and it is often necessary to shut down its browser or even shut the command down (`Ctrl-C`) and
restart it. No worries; it's pretty quick.

### End-to-end (E2E) Tests

E2E tests are in the `e2e` directory, side by side with the `app` folder.
Their filenames must end in `.e2e-spec.ts`.

Look for the example `e2e/app.e2e-spec.ts`.
Add more `.e2e-spec.js` files as you wish (although one usually suffices for small projects);
we configured protractor to find them.

Thereafter, run them with `npm run e2e`.

That command first compiles, then simultaneously starts the Http-Server at `localhost:8080`
and launches protractor.  

The pass/fail test results appear at the bottom of the terminal window.
A custom reporter (see `protractor.config.js`) generates a  `./_test-output/protractor-results.txt` file
which is easier to read; this file is excluded from source control.

Shut it down manually with `Ctrl-C`.